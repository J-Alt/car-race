$(function () {
  const car1 = $("#car1");
  const car2 = $("#car2");

  const car1Results = $(".car1-results");
  const car2Results = $(".car2-results");

  let done1 = false;
  let done2 = false;

  //   COUNTDOWN
  function countdown() {
    let c = 3;
    let interval = setInterval(function () {
      $(".count").show();
      $(".count p").text(c);
      if (c < 1) {
        clearInterval(interval);
        $(".count").hide();
      }
      c--;
    }, 1000);
  }

  //   FINISH
  function finish() {
    $(".end").delay(100).show();
  }
  //ENABLE BUTTONS
  function enableBtns() {
    if (done1 == true && done2 == true) {
      $("#start-over").attr("disabled", false);
      $("#raceBtn").attr("disabled", false);
    }
  }

  // PLAY THE GAME

  function drive() {
    countdown();
    done1 = false;
    done2 = false;

    $("#raceBtn").attr("disabled", true);
    $("#start-over").attr("disabled", true);

    let time1 = Math.floor(Math.random() * 5000);
    let time2 = Math.floor(Math.random() * 5000);
    let distance = $(window).width() - car1.width() - 15;
    let place1;
    let place2;

    if (time1 > time2) {
      place2 = "first";
      place1 = "second";
    } else {
      place1 = "first";
      place2 = "second";
    }

    car1.delay(4000).animate(
      {
        left: `${distance}px`,
      },
      time1,
      function () {
        finish();
        done1 = true;
        enableBtns();
        car1Results.append(
          `<tr class="border border-light text-center"><td class="py-2 ">Finished in: <span class="result white">${place1}</span> place with a time of: <span class="result white">${time1}</span> milliseconds!</td></tr>`
        );
      }
    );
    car2.delay(4000).animate(
      {
        left: `${distance}px`,
      },
      time2,
      function () {
        finish();
        done2 = true;
        enableBtns();
        car2Results.append(
          `<tr class="border border-light text-center"><td class="py-2 ">Finished in: <span class="result red">${place2}</span> place with a time of: <span class="result red">${time2}</span> milliseconds!</td></tr>`
        );
      }
    );

    let lastResults = {
      time1: time1,
      time2: time2,
      place1: place1,
      place2: place2,
    };

    let strLastResults = JSON.stringify(lastResults);
    localStorage.setItem("Last Results", strLastResults);
  }

  // EVENT LISTENERS
  $("#raceBtn").on("click", function () {
    drive();
  });

  $("#start-over").on("click", function () {
    $(".end").hide();
    car1.css("left", "");
    car2.css("left", "");
  });

  // ADD PREVIOUS RESULTS IF ANY
  if (localStorage.getItem("Last Results")) {
    let lastRes = JSON.parse(localStorage.getItem("Last Results"));

    $(".prev")
      .append(`<tr class="border border-light text-center"><td class="py-2 "><span class="result white">Car1</span> finished in: <span class="result white">${lastRes.place1}</span> place, with a time of <span class="result white">${lastRes.time1}</span> milliseconds!</td></tr>
	  <tr class="border border-light text-center"><td class="py-2 "><span class="result red">Car2</span> finished in: <span class="result red">${lastRes.place2}</span> place, with a time of <span class="result red">${lastRes.time2}</span> milliseconds!</td></tr>
	  `);
  }
});
